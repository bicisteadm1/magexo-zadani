FROM alpine:3.16.2

RUN apk add curl

WORKDIR /usr/src/app

COPY chucknorris.sh ./

RUN chmod +x chucknorris.sh

CMD ["sh", "chucknorris.sh"]